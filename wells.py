from dash import dash_table
from dash import dcc, html
import pandas as pd

#read data from csv
df_wells = pd.read_csv('Data/volve_wells.csv', sep=';')

#define wells page layout and content
page_layout = html.Div([
    dash_table.DataTable(
        id='wells-table',
        columns=[{"name": i, "id": i} for i in df_wells.columns],
        data=df_wells.to_dict("records"),  # Change is made here
        style_header={'font-family': 'Calibri', 'font-weight': 'bold'},
        style_cell={'padding':'10px', 'font-family': 'Calibri', 'font-size': '16px', 'width': '100px'}
    )], style={'width':'70%'})
